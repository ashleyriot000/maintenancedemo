﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BNG {

    public enum MovementVector {
        HMD,
        Controller
    }    

    public class SmoothLocomotion : MonoBehaviour {

        public float MovementSpeed = 1.25f;
        public float SprintSpeed = 1.5f;

        /// <summary>
        /// The key(s) to use to enabled Sprint. Override SprintKeyDown() if these don't fit your needs
        /// </summary>
        [Tooltip("The key(s) to use to initiate sprint. You can also override the SprintKeyDown() function to determine your sprint criteria.")]
        public List<ControllerBinding> SprintInput = new List<ControllerBinding>() { ControllerBinding.None };

        public float StrafeSpeed = 1f;
        public float StrafeSprintSpeed = 1.25f;

        [Tooltip("Should world gravity be applied to the player?")]
        public bool ApplyGravity = true;

        [Tooltip("Multiply World Gravity by this amount")]
        public float GravityModifier = 0.3f;

        /// <summary>
        /// The key(s) to use to enabled Sprint. Override SprintKeyDown() if these don't fit your needs
        /// </summary>
        [Tooltip("The key(s) to use to initiate a jump. You can also override the CheckJump() function to determine your jump criteria.")]
        public List<ControllerBinding> JumpInput = new List<ControllerBinding>() { ControllerBinding.None };

        [Tooltip("Amount of 'force' to apply to the player during Jump")]
        public float JumpForce = 3f;

        [Tooltip("Can the player move when not grounded? Set to true if you want to be able to move the joysticks and have the player respond to input even when not grounded.")]
        public bool AirControl = true;

        public bool AllowKeyboardInputs = true;

        BNGPlayerController playerController;
        CharacterController characterController;

        // Left / Right
        float movementX;

        // Up / Down
        float movementY;

        // Forwards / Backwards
        float movementZ;

        bool movementDisabled = false;

        private float _verticalSpeed = 0; // Keep track of vertical speed

        void Start() {
            characterController = GetComponent<CharacterController>();
            playerController = GetComponent<BNGPlayerController>();
            if(playerController == null) {
                playerController = GetComponentInParent<BNGPlayerController>();
            }
        }

        // Update is called once per frame
         void Update() {
            updateInputs();            
            moveCharacter();
        }

        void updateInputs() {

            // Start by resetting our previous frame's inputs
            movementX = 0;
            movementY = 0;
            movementZ = 0;

            // Start with VR Controller Input
            Vector2 primaryAxis = InputBridge.Instance.LeftThumbstickAxis;
            if (playerController.IsGrounded() || AirControl) {
                movementX = primaryAxis.x;
                movementZ = primaryAxis.y;
            }

            // If VR Inputs not in use, check for keyboard inputs
            if(AllowKeyboardInputs && movementX == 0 && movementZ == 0) {
                GetKeyBoardInputs();
            }
            
            if(CheckJump()) {
                movementY += JumpForce;
            }

            if(SprintKeyDown()) {
                movementX *= StrafeSprintSpeed;
                movementZ *= SprintSpeed;
            }
            else {
                movementX *= StrafeSpeed;
                movementZ *= MovementSpeed;
            }            
        }

        void moveCharacter() {

            if(movementDisabled) {
                return;
            }

            // Apply gravity to Y
            if (ApplyGravity && !playerController.IsGrounded() && !playerController.GrippingClimbable && playerController.GravityEnabled) {
                movementY -= playerController.GravityAmount * GravityModifier;
            }

            Vector3 moveDirection = new Vector3(movementX, movementY, movementZ);
            moveDirection = transform.TransformDirection(moveDirection);

            // Check for jump value
            if (playerController.IsGrounded() && !movementDisabled) {
                // Reset jump speed if grounded
                _verticalSpeed = 0;
                if (CheckJump()) {
                    _verticalSpeed = JumpForce;
                }
            }
            _verticalSpeed -= playerController.GravityAmount * Time.deltaTime;

            moveDirection.y = _verticalSpeed;

            playerController.LastPlayerMoveTime = Time.time;

            characterController.Move(moveDirection * Time.deltaTime);
        }

        public virtual void GetKeyBoardInputs() {
            // Forward
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) {
                movementZ += 1f;
            }
            // Back
            if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) {
                movementZ -= 1f;
            }
            // Left
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) {
                movementX -= 1f;
            }
            // Right
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) {
                movementX += 1f;
            }
        }

        public virtual bool CheckJump() {

            // Don't jump if not grounded
            if(!playerController.IsGrounded()) {
                return false;
            }

            // Check for bound controller button
            for (int x = 0; x < JumpInput.Count; x++) {
                if (InputBridge.Instance.GetControllerBindingValue(JumpInput[x])) {
                    return true;
                }
            }

            // Keyboard input
            if (AllowKeyboardInputs && Input.GetKeyDown(KeyCode.Space)) {
                return true;
            }

            return false;
        }

        public virtual bool SprintKeyDown() {

            // Allow Keyboard Sprinting
            if(AllowKeyboardInputs &&  (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.LeftShift))) {
                return true;
            }

            // Check for bound controller button
            for (int x = 0; x < SprintInput.Count; x++) {
                if (InputBridge.Instance.GetControllerBindingValue(SprintInput[x])) {
                    return true;
                }
            }

            return false;
        }

        public virtual void EnableMovement() {
            movementDisabled = false;
        }

        public virtual void DisableMovement() {
            movementDisabled = true;
        }
    }
}

