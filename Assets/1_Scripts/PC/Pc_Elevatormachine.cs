﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BNG;

public class Pc_Elevatormachine : GrabbableEvents 
{

    CharacterController characterController;
    BNGPlayerController bngController;


    public Transform machine;



    public float movespeed;

    public Vector3 origin;

    public Transform myBody;


    private void Start()
    {
        origin = machine.transform.position;
        characterController = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<CharacterController>();
        bngController = GameObject.FindGameObjectWithTag("Player").GetComponent<BNGPlayerController>();
    }

    public override void OnGrab(Grabber grabber)
    {
        // enforce gravity

        bodytran();

    }

    public override void OnRelease()
    {
        // enforce gravity
        exitbody();
    }

    

    public void bodytran()
    {
        characterController.enabled = false;
        myBody.parent = machine;

        characterController.enabled = false;

    }

    public void exitbody()
    {
        characterController.enabled = true;
        myBody.parent = null;

    }


    public void UP_move()
    {
        machine.transform.localPosition = Vector3.MoveTowards(machine.transform.localPosition, 
            new Vector3(machine.transform.localPosition.x,origin.y + 3, machine.transform.localPosition.z) , movespeed * Time.deltaTime);


      //  bodytran();

    }
    public void Down_move()
    {
        machine.transform.localPosition = Vector3.MoveTowards(machine.transform.localPosition,
            new Vector3(machine.transform.localPosition.x, origin.y, machine.transform.localPosition.z), movespeed * Time.deltaTime);

      //  bodytran();
    }
    public void Left_move()
    {
        machine.transform.localPosition = Vector3.MoveTowards(machine.transform.localPosition,
            new Vector3(origin.x-3, machine.transform.localPosition.y, machine.transform.localPosition.z), movespeed * Time.deltaTime);
     //   bodytran();
    }
    public void Right_move( )
    {
        machine.transform.localPosition = Vector3.MoveTowards(machine.transform.localPosition,
            new Vector3(origin.x + 3, machine.transform.localPosition.y, machine.transform.localPosition.z), movespeed  * Time.deltaTime);
      //  bodytran();
    }
    public void Forward_move( )
    {
        machine.transform.localPosition = Vector3.MoveTowards(machine.transform.localPosition,
            new Vector3(machine.transform.localPosition.x, machine.transform.localPosition.y,origin.z +3), movespeed  * Time.deltaTime);
     //   bodytran();
    }
    public void Back_move( )
    {
        machine.transform.localPosition = Vector3.MoveTowards(machine.transform.localPosition,
            new Vector3(machine.transform.localPosition.x, machine.transform.localPosition.y, origin.z-3), movespeed  * Time.deltaTime);
      //  bodytran();
    }


}
