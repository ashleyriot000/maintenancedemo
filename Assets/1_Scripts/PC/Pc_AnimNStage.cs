﻿using BNG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BNG;

public class Pc_AnimNStage : GrabbableEvents
{

    public GameObject gear;

    public Material holographic;

    public Transform targetTransform;


    private void Start()
    {

        gear.GetComponent<Rigidbody>().isKinematic = true;
        gear.GetComponent<Grabbable>().enabled = false;
        gear.GetComponent<GrabbableRingHelper>().enabled = false;
    }
    public void playanima()
    {
        gear.GetComponent<Animator>().SetTrigger("Next_Step");
    }

    public void fallDown()
    {
        gear.transform.parent = null;
        gear.GetComponent<Rigidbody>().isKinematic = false;
        gear.GetComponent<Grabbable>().enabled = true;
        gear.GetComponent<GrabbableRingHelper>().enabled = true;
        
    }


    public override void OnGrip(float gripValue)
    {
        base.OnGrip(gripValue);

        combinecheck();
    }

    public void combinecheck()
    {
        float dis = Vector3.Distance(targetTransform.position, gear.transform.position);


        if (dis < 0.2f)
        {
            targetTransform.gameObject.SetActive(true);
           

            gear.GetComponent<Rigidbody>().isKinematic = true;
            gear.GetComponent<Grabbable>().enabled = false;
            gear.GetComponent<GrabbableRingHelper>().enabled = false;

            gear.SetActive(false);
        }
    }


}
