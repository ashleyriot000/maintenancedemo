﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pc_Combine : MonoBehaviour
{
    public Transform combine_obj;
    public Transform target_obj;
    public Transform Image_obj;

    private void FixedUpdate()
    {
        Act_combine();
    }
    public void Act_combine()
    {
        float dis = Vector3.Distance(target_obj.position, combine_obj.position);


        if(dis < 0.3f)
        {
            target_obj.gameObject.SetActive(true);
            combine_obj.gameObject.SetActive(false);
            Image_obj.gameObject.SetActive(false);

        }
    }
}
