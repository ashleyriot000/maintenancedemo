﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PC_vibe : MonoBehaviour
{
    
   public void viberation()
    { 
        OVRInput.SetControllerVibration(0.016f, 1f, OVRInput.Controller.RTouch);
        OVRInput.SetControllerVibration(0.016f, 1f, OVRInput.Controller.LTouch);

    }
    public void stopviberation()
    {
        OVRInput.SetControllerVibration(0, 0, OVRInput.Controller.RTouch);
        OVRInput.SetControllerVibration(0, 0, OVRInput.Controller.LTouch);
    }
}
