﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DK_Training_UIBtn : Button 
{
    Text[] m_Text;
    bool is_init = false;
    public void Set_Text(string idx, string name, string date) {

        if(!is_init) {
            m_Text = new Text[3];
            m_Text[0] = transform.GetChild(0).GetComponent<Text>();
            m_Text[1] = transform.GetChild(1).GetComponent<Text>();
            m_Text[2] = transform.GetChild(2).GetComponent<Text>();
            is_init = true;
        }
        
        m_Text[0].text = idx;
        m_Text[1].text = name;
        m_Text[2].text = date;
    }
}
