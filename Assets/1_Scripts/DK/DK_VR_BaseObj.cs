﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void myEventFunction();
public class DK_VR_BaseObj : MonoBehaviour {
    
    public myEventFunction m_Event;

    protected bool m_Active;
    public bool Active { get { return m_Active; } set { m_Active = value; } }

    protected bool m_Clear;
    public bool Clear { get { return m_Clear; } }

    public void Set_EventFunction(myEventFunction del)
    {
        m_Event = del;
    }
}
