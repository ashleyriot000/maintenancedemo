﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

/// <summary>
/// Singleton Stage Manager Class
/// </summary>
public class DK_StageManager : MonoBehaviour {


    private static DK_StageManager m_Inst;
    private void Awake() {
        if (m_Inst == null) {
            m_Inst = this;
            DontDestroyOnLoad(this.gameObject);
        } else Destroy(this);
    }
    
    public static DK_StageManager Inst {
        get { return m_Inst; }
    }

    public string UserName;
    public string lastScore;
    public string lastTime;

    [SerializeField]
    private DK_ClearUI m_ClearUI;
    [SerializeField]
    private DK_Base_Stage[] m_Stages;
    [SerializeField]
    private DK_Base_Stage m_CurStage;
    private DK_Base_Stage m_lastStage;
    [SerializeField]
    private GameObject[] m_NextTel;
    private int curStageNum = 0;
    private float m_ClearTime = 0;
    protected bool m_Stage;
    public bool Stage { set { m_Stage = value; } get { return m_Stage; } }
    public bool Check_StageClear() {
        return Stage;
    }

    public void Get_Next() {
        if (m_NextTel.Length > curStageNum) {
            m_lastStage = m_CurStage;
            m_NextTel[curStageNum++].SetActive(true);
            m_CurStage = m_Stages[curStageNum];
            m_CurStage.Enter();
            m_CurStage.Is_Active(true);
            m_lastStage.Is_Active(false);
        }
    }

    private void Start() {
        curStageNum = 0;
        m_CurStage = m_Stages[curStageNum];
        m_CurStage.Enter();
        m_CurStage.Is_Active(true);
    }

    private void Update() {
        if (Stage) return;


        foreach (DK_Base_Stage temp in m_Stages) {
            temp.Clear_Update();
        }

        foreach (DK_Base_Stage temp in m_Stages) {
            temp.Clear_Update();
            if (!temp.Clear) {
                m_ClearTime += Time.deltaTime;
                return;
            } 
        }

        // 클리어 화면 띄우기 m_ClearUI -> 사용자이름 + 점수 + 시간 표시
        Stage = true;
        Debug.Log("Claer");
        m_ClearUI.Set_UI(UserName, ClearScore(), m_ClearTime);
        StartCoroutine(DK_Php_Network.Instance.PlayerPrefabs_Save(UserName, ClearScore(), m_ClearTime));
    }

    public int ClearScore() {
        int score=0;
        foreach (DK_Base_Stage temp in m_Stages) {
            if (temp.Clear) {
                score += temp.Score;
            }
        }
        return score;
    }

    public void Set_Data(string name, string score, string time) {
        UserName = name;
        lastScore = score;
        lastTime = time;
    }
}
