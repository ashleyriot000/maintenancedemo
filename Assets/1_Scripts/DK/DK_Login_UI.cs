﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DK_Login_UI : DK_VR_BaseObj {
    public float y_value;

    [SerializeField] Canvas m_canvs;
    [SerializeField] GameObject m_NewProfile;
    [SerializeField] GameObject m_Content;
    [SerializeField] Button[] m_CustomBtn;
    [SerializeField] List<Button> m_UserBtn = new List<Button>(0);

    [SerializeField] GameObject m_Create_UI;
    [SerializeField] GameObject m_Select_UI;
    [SerializeField] InputField m_Datas;
   

    private void Start() {
        m_canvs.worldCamera = Camera.main;
        StartCoroutine(Create_Profile_Cour());
        //m_CustomBtn[0].onClick.AddListener(() => Create_Profile_UI(true));
        //m_CustomBtn[1].onClick.AddListener(() => Create_Profile_UI(false));
        //m_CustomBtn[2].onClick.AddListener(() => Create_New_User());
    }
    public void Create_Profile_UI(bool on_off) {
        m_Create_UI.SetActive(on_off);
        m_Select_UI.SetActive(!on_off);
    }
    public void Create_New_User() {
        List<Php_Field<string>> m_data = new List<Php_Field<string>>(0);
        m_data.Add(new Php_Field<string>("name", m_Datas.text));
        m_data.Add(new Php_Field<string>("test1_score", "0"));
        m_data.Add(new Php_Field<string>("test2_score", "0"));
        DK_Php_Network.Instance.Insert(m_data);

        Create_Profile_UI(false);
        StartCoroutine(Create_Profile_Cour());
        //m_Datas.text = "";
    }
    public void Create_Profile() {
        DK_Php_Network.Instance.Demo_Data_Select();
        float yValue = 0;
        Debug.Log(DK_Php_Network.Instance.DataBase.Count);
        int idx = 1;
        foreach (string[] temp in DK_Php_Network.Instance.DataBase)
        {
            GameObject _temp = Instantiate(m_NewProfile, new Vector3(0,yValue,0), Quaternion.identity);
            DK_Training_UIBtn _btn = _temp.GetComponent<DK_Training_UIBtn>();
            m_UserBtn.Add(_btn);
            _btn.transform.SetParent(m_Content.transform);
            _btn.transform.localScale = new Vector3(1, 1, 1);
            _btn.onClick.AddListener(() => Set_Name(temp[0],temp[1],temp[2]));
            _btn.Set_Text(idx.ToString(), temp[0], temp[3]);
            yValue -= y_value;
            idx++;
        }
        /*m_CustomBtn[0].transform.position = new Vector3(0, yValue, 0);
        m_CustomBtn[0].transform.rotation = Quaternion.identity;
        m_CustomBtn[0].transform.localScale = new Vector3(1, 1, 1);
        m_CustomBtn[0].transform.SetParent(m_Content.transform);*/
    }
    public IEnumerator Create_Profile_Cour() {
        float yValue = 0;
        if (m_UserBtn.Count > 0) yield return Destroy_Profile();

        // 웹서버 완성시
        /* yield return DK_Php_Network.Instance.DB_Select();
         foreach (string[] temp in Php_Network.Instance.DataBase) {
             GameObject _temp = Instantiate(m_NewProfile);
             Button _btn = _temp.GetComponent<Button>();
             m_UserBtn.Add(_btn);
             _btn.transform.SetParent(m_Content.transform);
             _btn.transform.localPosition = new Vector3(0, yValue, 0);
             _btn.transform.localScale = new Vector3(1, 1, 1);
             _btn.GetComponentInChildren<Text>().text = temp[0];
             _btn.onClick.AddListener(() => Set_Name(temp[0], temp[1], temp[2]));

             yValue -= y_value;
         }*/

        int idx = 1;
        yield return DK_Php_Network.Instance.PlayerPrefabs_Select();
        foreach (string[] temp in DK_Php_Network.Instance.DataBase) {
            GameObject _temp = Instantiate(m_NewProfile);
            DK_Training_UIBtn _btn = _temp.GetComponent<DK_Training_UIBtn>();
            m_UserBtn.Add(_btn);
            _btn.transform.SetParent(m_Content.transform);
            _btn.transform.localPosition = new Vector3(0, yValue, 0);
            _btn.transform.localScale = new Vector3(1, 1, 1);
            _btn.transform.localEulerAngles = Vector3.zero;
            _btn.GetComponentInChildren<Text>().text = temp[0];
            _btn.onClick.AddListener(() => Set_Name(temp[0], temp[1], temp[2]));
            _btn.Set_Text(idx.ToString(), temp[0], temp[3]);
            idx++;
            yValue -= y_value;
        }
        
        /*m_CustomBtn[0].transform.localPosition = new Vector3(0, yValue, 0);
        m_CustomBtn[0].transform.localEulerAngles = Vector3.zero;
        m_CustomBtn[0].transform.localScale = new Vector3(1, 1, 1);
        m_CustomBtn[0].transform.SetParent(m_Content.transform);*/
        yield return null;
    }
    public IEnumerator Destroy_Profile() {
        foreach (Button temp in m_UserBtn) {
            GameObject.Destroy(temp.gameObject);
        }
        m_UserBtn.Clear();
        yield return null;
    }
    public void Set_Name(string _name, string test1, string test2) {
        Debug.Log("Set_Name : " + _name + " " + test1 + " " + test2);
        if (!m_Clear) {
            // 현재 값 넘겨주기
            Debug.Log("Set_Name : " + _name + " " + test1 + " " + test2);
            // 다음스테이지 넘어가기
            m_Clear = true;
        }
    }
}