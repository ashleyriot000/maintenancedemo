﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BNG;

public class DK_Base_Slot : MonoBehaviour
{
    #region _ 변 수 _
    public bool SlotCheck { get { return Is_Check; } set { Is_Check = value; } }
    [Tooltip("체크됬나확인")]
    public bool Is_Check;
    public string SlotName { get { return m_SlotName; } set { m_SlotName = value; } }
    [Tooltip("슬롯이름")]
    public string m_SlotName;
    public string Type { get { return m_Type; } set { m_Type = value; } }
    [Tooltip("타입설정")]
    public string m_Type;
    [Tooltip("셋팅된 리스트")]
    public DK_Base_Slot_Object m_SlotObj;
    #endregion

    #region _ Unity _
    public virtual void FixedUpdate() {
        if (m_SlotObj == null) return;
        /*else if(m_SlotObj.SlotCheck) 
        {
            m_SlotObj.transform.position = this.transform.position;
        }*/

        /*if(!m_SlotObj.SlotCheck) {
            m_SlotObj.Out_Slot(true);
            m_SlotObj = null;
            Is_Check = false;
            return;
        }*/

        if(m_SlotObj.SlotCheck && !Is_Check) {
            // 슬롯장착
            Debug.Log("슬롯장착");
            m_SlotObj.In_Slot(this.transform, false);
            Is_Check = true;
        }
        
    }
    public virtual void OnTriggerEnter(Collider other) {
        if (m_SlotObj != null) return;

        if (Check_Enter(other)) {
        }
    }
    private void OnTriggerStay(Collider other) {
        if(m_SlotObj != null) return;

        if (Check_Enter(other)) {
        }
    }
    public virtual void OnTriggerExit(Collider other) {
        if (m_SlotObj == null) return;

        if (Check_Exit(other)) {
            m_SlotObj.Out_Slot(true);
            m_SlotObj = null;
            Is_Check = false;
        }
    }
    #endregion

    protected bool Check_Enter(Collider other) {
        DK_Base_Slot_Object temp = other.transform.GetComponent<DK_Base_Slot_Object>();
        if (temp == null) {
            return false; 
        }

        if (!temp.SlotName.Equals(SlotName) 
            || !temp.SlotCheck) {
            return false; 
        }

        m_SlotObj = temp;
        return true;
    }
    protected bool Check_Exit(Collider other) {
        DK_Base_Slot_Object temp = other.transform.GetComponent<DK_Base_Slot_Object>();

        if (temp == null) return false;

        if (m_SlotObj == temp) {
            return true;
        }

        return false;
    }
}