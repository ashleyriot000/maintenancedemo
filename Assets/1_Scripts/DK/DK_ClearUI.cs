﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DK_ClearUI : MonoBehaviour
{
    public Text[] m_Tests;

    public void Set_UI(string name, int score, float time,Transform target) {
        var minute = time % 3600 / 60;
        var second = time % 3600 % 60;
        m_Tests[0].text = name;
        m_Tests[1].text = score.ToString();
        m_Tests[2].text = string.Format("{0:D2}:{1:D2}", (int)minute, (int)second);
        gameObject.SetActive(true);
    }
    public void Set_UI(string name, int score, float time) {
        var minute = time % 3600 / 60;
        var second = time % 3600 % 60;
        m_Tests[0].text = name;
        m_Tests[1].text = score.ToString();
        m_Tests[2].text = string.Format("{0:D2}:{1:D2}", (int)minute, (int)second);
        gameObject.SetActive(true);
    }
}
