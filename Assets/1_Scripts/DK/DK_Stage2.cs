﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DK_Stage2 : DK_Base_Stage 
{
    public override void Clear_Update() {
        if (Clear) return;

        if (m_ClearObj.Clear) {

            Score = 100;
            m_Clear = true;

            DK_StageManager.Inst.Get_Next();
        }
    }
}
