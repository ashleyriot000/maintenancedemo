﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using BNG;
using UnityEngine.Rendering;

public class DK_Base_Slot_Object : MonoBehaviour {
    [SerializeField]
    Grabbable m_Grab;
    #region _ 변 수 _
    public bool SlotCheck { get { return Is_Check; } set { Is_Check = value; } }
    [Tooltip("체크됬나확인")]
    public bool Is_Check;
    public string ItemName { get { return m_ItemName; } set { m_ItemName = value; } }
    [Tooltip("체크됬나확인")]
    public string m_ItemName;
    public string SlotName { get { return m_SlotName; } set { m_SlotName = value; } }
    [Tooltip("슬롯이름")]
    public string m_SlotName;
    public string Type { get { return m_Type; } set { m_Type = value; } }
    [Tooltip("타입설정")]
    public string m_Type;
    public float m_DelayTime;
    public DK_Base_Slot m_Slot;
    public Rigidbody rigid;
    public BoxCollider coll;
    #endregion
    private void Awake() {
        rigid = GetComponent<Rigidbody>();
        coll = GetComponent<BoxCollider>();
    }
    public void In_Slot(Transform _trans, bool _is) {
        //rigid.useGravity = _is;
        rigid.velocity = Vector3.zero;
        rigid.angularVelocity = Vector3.zero;
        rigid.Sleep();

        transform.parent = _trans;
        transform.position = _trans.position;
        transform.rotation = _trans.rotation;
    }
    public void Out_Slot(bool _is) {
        transform.rotation = Quaternion.identity;

        //rigid.useGravity = _is;
        rigid.velocity = Vector3.zero;
        rigid.angularVelocity = Vector3.zero;
        rigid.Sleep();
    }
    private void OnTriggerEnter(Collider other) {
        DK_Base_Slot slot = other.GetComponent<DK_Base_Slot>();
        if (slot == null) return;

        if (slot.SlotName == SlotName && !SlotCheck) {
            Debug.Log("슬롯입장");
            m_Slot = slot;
            rigid.useGravity = false;
            Out_Slot(false);
        }
    }
    private void OnTriggerStay(Collider other) {
        DK_Base_Slot slot = other.GetComponent<DK_Base_Slot>();
        if (slot == null) return;

        if (slot.SlotName == SlotName && !SlotCheck) {
            rigid.useGravity = false;
            Out_Slot(false);
            //m_DelayTime += Time.deltaTime;
            Debug.Log("드릴질");
            if (m_DelayTime >= 3) {
                gameObject.layer = 0;
                rigid.useGravity = false;
                SlotCheck = true;
                m_Grab.enabled = false;
                coll.isTrigger = true;
                Debug.Log("드릴완료");
            }
        }
        else if (slot.SlotName == SlotName && SlotCheck) {
            rigid.useGravity = false;
            Out_Slot(false);
            Debug.Log("드릴질");
            if (m_DelayTime <= 0) {
                gameObject.layer = 13;
                rigid.useGravity = true;
                transform.parent = null;
                SlotCheck = false;
                m_DelayTime = 0;
                m_Slot = null;
                Out_Slot(false);
                m_Grab.enabled = true;
                coll.isTrigger = false;
                Debug.Log("드릴완료");
            }
        }
    }

    public virtual void OnTriggerExit(Collider other) {


        DK_Base_Slot temp = other.transform.GetComponent<DK_Base_Slot>();

        if (temp == null) return;

        if (m_Slot == temp) {
            gameObject.layer = 13;
            rigid.useGravity = true;
            transform.parent = null;
            SlotCheck = false;
            m_DelayTime = 0;
            m_Slot = null;
            Out_Slot(false);
            return;
        }
    }
    protected bool Check_Exit(Collider other) {
        DK_Base_Slot temp = other.transform.GetComponent<DK_Base_Slot>();

        if (temp == null) return false;

        if (m_Slot == temp) {
            gameObject.layer = 13;
            SlotCheck = false;
            m_DelayTime = 0;
            m_Slot = null;
            return true;
        }

        return false;
    }
}
