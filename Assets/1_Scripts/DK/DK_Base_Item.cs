﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DK_Base_Item : MonoBehaviour
{
    public string ItemName { get { return m_ItemName; } set { m_ItemName = value; } }
    public string m_ItemName;
    public DK_Base_Slot_Object m_SlotObj;
    public float m_power = 1.0f;
    public bool m_Active;
    private void Update() {
        if (OVRInput.Get(OVRInput.Button.SecondaryIndexTrigger, OVRInput.Controller.Touch)) {
            Debug.Log("드릴작동");
            m_Active = true;
        } else m_Active = false;

        if (OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.RTouch)) {
            Debug.Log("모드바꾸기");
            m_power *= -1;
        }
    }
    private void OnTriggerEnter(Collider other) {
        DK_Base_Slot_Object temp = other.GetComponent<DK_Base_Slot_Object>();
        if (temp == null) return;

        m_SlotObj = temp;
    }
    private void OnTriggerStay(Collider other) {
        if (m_SlotObj == null) return;

        if (m_SlotObj.gameObject != other.gameObject) return;

        if (m_SlotObj.ItemName.Equals(ItemName) && m_Active) {
            m_SlotObj.m_DelayTime += Time.deltaTime * m_power;
        }
    }

    public virtual void OnTriggerExit(Collider other) {
        DK_Base_Slot_Object temp = other.GetComponent<DK_Base_Slot_Object>();
        if (temp == null) return;
        if(temp==m_SlotObj) {
            m_SlotObj = null;
        }
    }
}
