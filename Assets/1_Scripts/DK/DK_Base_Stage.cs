﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 공장 조건 관리
/// </summary>
public class DK_Base_Stage : MonoBehaviour
{
    [SerializeField] protected DK_VR_BaseObj[] m_Objects;
    [SerializeField] protected DK_VR_BaseObj m_ClearObj;
    public int Score;

    public bool m_Active;
    public bool Active { get { return m_Active; }}

    public bool m_Clear;
    public bool Clear { get { return m_Clear; } }

    public virtual void Clear_Update() { }
    public void Is_Active(bool _is) {
        for(int i = 0; i < m_Objects.Length; i++) {
            m_Objects[i].Active = _is;
        }
        m_ClearObj.Active = _is;
        m_Active = _is;
    }
    public virtual void Enter() {
        Score = 0;
        m_Clear = false;
    }
}