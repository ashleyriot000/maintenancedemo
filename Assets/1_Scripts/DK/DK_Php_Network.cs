﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DK_Php_Network : MonoBehaviour {

    private static DK_Php_Network instance = null;

    void Awake() {
        if (null == instance) {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        } else {
            Destroy(this.gameObject);
        }
    }

    public static DK_Php_Network Instance {
        get {
            if (null == instance) {
                return null;
            }
            return instance;
        }
    }

    private string url_static = "http://localhost/";
    private string url = "User_Create.php";


    public List<Php_Field<string>> fieldData = new List<Php_Field<string>>(0);

    [SerializeField]
    public List<string[]> DataBase = new List<string[]>(0);
    public bool is_Select = false;

    public void Demo_Data_Select() {
        //StartCoroutine(DB_Select());
        StartCoroutine(PlayerPrefabs_Select());
    }

    public IEnumerator DB_Select() {

        url = "Demo_View.php";
        UnityWebRequest uwr = UnityWebRequest.Get(url_static + url);
        yield return uwr.SendWebRequest();

        if (uwr.isNetworkError)
            Debug.Log("연결 앙댐");
        else {
            yield return View(uwr.downloadHandler.text);
            is_Select = true;
        }

        yield return null;
    }

    public IEnumerator PlayerPrefabs_Select() {
        string demo;
        if (PlayerPrefs.GetString("demo_data") == string.Empty) {
            demo = "김덕은,0,0,2020-08-11;이평화,0,0,2020-08-11;";
            PlayerPrefs.SetString("demo_data", demo);
        }
        demo = PlayerPrefs.GetString("demo_data");
        yield return PlayerPrefabs_View(demo);
    }
    private IEnumerator PlayerPrefabs_Insert() {
        string data = PlayerPrefs.GetString("demo_data");
        for (int i = 0; i < fieldData.Count; i++) {
            data += fieldData[i].Value;
            if (i == fieldData.Count - 1) data += ";";
            else data += ",";
        }
        PlayerPrefs.SetString("demo_data", data);
        yield return null;
    }
    public IEnumerator PlayerPrefabs_View(string _data) {
        DataBase.Clear();
        string[] rows = _data.Split(';');
        string[] cloumn = new string[0];
        for (int i = 0; i < rows.Length - 1; i++) {
            cloumn = rows[i].Split(',');
            DataBase.Add(cloumn);
        }
        yield return null;
    }
    public IEnumerator PlayerPrefabs_Save(string _name, int _score, float _time) {
        string saveData = string.Empty;
        foreach(string[] temp in DataBase) {
            if(string.Equals(temp[0],_name)) {
                temp[1] = _score.ToString();
                temp[2] = _time.ToString();
            }
            saveData += temp[0]+","+temp[1]+","+temp[2]+","+ DateTime.Now.ToString("yyyy-MM-dd")+";";
        }
        PlayerPrefs.SetString("demo_data", saveData);

        yield return null;
    }

    private IEnumerator DB_Insert() {
        url = "Demo_Create.php";

        WWWForm form = new WWWForm();
        string data = string.Empty;
        for (int i = 0; i < fieldData.Count; i++) { 
            form.AddField(fieldData[i].Name, fieldData[i].Value);
            data += fieldData[i].Value + ",";
        }
        Debug.Log(data);
        UnityWebRequest uwr = UnityWebRequest.Post(url_static + url, form);
        yield return uwr.SendWebRequest();

        if (uwr.isNetworkError)
            Debug.Log("연결 앙댐");
        else
            Debug.Log("Receive" + uwr.downloadHandler.text);
    }
    private IEnumerator DB_Edit() {
        url = "Demo_Update.php";

        WWWForm form = new WWWForm();

        for (int i = 0; i < fieldData.Count; i++)
            form.AddField(fieldData[i].Name, fieldData[i].Value);

        UnityWebRequest uwr = UnityWebRequest.Post(url_static + url, form);
        yield return uwr.SendWebRequest();

        if (uwr.isNetworkError)
            Debug.Log("연결 앙댐");
        else
            Debug.Log("Receive" + uwr.downloadHandler.text);
    }
    private IEnumerator DB_Delete() {
        url = "Demo_Delete.php";

        WWWForm form = new WWWForm();

        for (int i = 0; i < fieldData.Count; i++)
            form.AddField(fieldData[i].Name, fieldData[i].Value);

        UnityWebRequest uwr = UnityWebRequest.Post(url_static + url, form);
        yield return uwr.SendWebRequest();

        if (uwr.isNetworkError)
            Debug.Log("연결 앙댐");
        else
            Debug.Log("Receive" + uwr.downloadHandler.text);
    }

    public IEnumerator View(string _data) {
        DataBase.Clear();
        string[] rows = _data.Split(';');
        string[] cloumn = new string[0];
        for (int i = 0; i < rows.Length - 1; i++) {
            cloumn = rows[i].Split(',');
            DataBase.Add(cloumn);
        }

        string FF = string.Empty;
        foreach (string[] temp in DataBase) {
            for (int i = 0; i < temp.Length; i++) {
                FF += temp[i];
                FF += ",";
            }
            FF += ";";
        }
        yield return null;
    }
    public void Insert(List<Php_Field<string>> _fieldData) {
        fieldData = _fieldData;
        StartCoroutine(PlayerPrefabs_Insert());
    }
}

public class Php_Field<T> {
    public string Name;
    public T Value;

    public Php_Field(string _name, T value) {
        Name = _name;
        Value = value;
    }
}