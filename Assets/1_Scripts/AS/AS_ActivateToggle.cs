﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AS_ActivateToggle : MonoBehaviour
{
  public void SetActiveInverse()
  {
    gameObject.SetActive(!gameObject.activeSelf);
  }
}
