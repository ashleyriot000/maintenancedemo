﻿using Photon.Pun.Demo.Asteroids;
using System.Collections;
using System.Runtime.Remoting.Messaging;
using UnityEngine;

public class AS_AssembleBundle : MonoBehaviour
{
  public enum BundleType
  {
    Grabbable,
    NoneGrabbable
  }
  [SerializeField] BundleType m_type = BundleType.Grabbable;
  [SerializeField] GameObject m_emission;
  [SerializeField] int m_assembleID;
  [SerializeField] bool m_grabbed;
  [SerializeField] bool m_selectedSameID;
  [SerializeField] bool m_relatedGrabbed;
  [SerializeField] AS_AssemblePoint[] m_points;

  public BundleType GetBundleType
  {
    get
    {
      return m_type;
    }
  }

  public bool Grabbed
  {
    get
    {
      return m_grabbed;
    }
    set
    {
      if (m_grabbed == value)
        return;
      m_grabbed = value;
      UpdateState();
    }
  }
  public bool SelectedSameID
  {
    get
    {
      return m_selectedSameID;
    }
    set
    {
      if (m_selectedSameID == value)
        return;
      m_selectedSameID = value;
      UpdateState();
    }
  }
  public bool RelatedGrabbed
  {
    get
    {
      return m_relatedGrabbed;
    }
    set
    {
      if (m_relatedGrabbed == value)
        return;
      m_relatedGrabbed = value;
      UpdateState();
    }
  }

  private void Start()
  {
    if (m_type == BundleType.NoneGrabbable)
    {
      Grabbed = true;
      var pieces = FindObjectsOfType<AS_AssemblePiece>();
      int id = -1;
      foreach (AS_AssemblePoint p in m_points)
      {
        p.GrabbedBundle = true;
        if (id != p.AssembleableID)
          id = p.AssembleableID;
      }
    }
    m_emission.SetActive(false);
  }
  private void UpdateState()
  {
    if(m_type == BundleType.Grabbable)
    {
      if (m_grabbed)
      {
        m_emission.SetActive(false);
      }
      else
      {
        if (m_relatedGrabbed && !m_selectedSameID) m_emission.SetActive(true);
        else m_emission.SetActive(false);
      }
    }    
  }

  public void OnGrab(bool grabbed)
  {
    Grabbed = grabbed;
    var pieces = FindObjectsOfType<AS_AssemblePiece>();
    int id = -1;
    foreach(AS_AssemblePoint p in m_points)
    {
      p.GrabbedBundle = grabbed;
      if (id != p.AssembleableID)
        id = p.AssembleableID;
      foreach(AS_AssemblePiece piece in pieces)
      {
        if (piece.AssembleID != id) return;

        piece.RelatedGrabbed = grabbed;
      }
    }
    var bundles = FindObjectsOfType<AS_AssembleBundle>();
    foreach(AS_AssembleBundle b in bundles)
    {
      b.SelectedSameID = grabbed;
    }
  }

  public bool RelativeGrab(bool grabbed, int assembleID)
  {
    bool relative = false;
    foreach(AS_AssemblePoint p in m_points)
    {
      if (p.AssembleableID != assembleID)
        continue;
      relative = true;
      p.GrabbedSameID = grabbed;
    }
    return relative ? grabbed : false;
  }
}