﻿using BNG;
using UnityEngine;

public class AS_AssemblePoint : MonoBehaviour
{
  [SerializeField] int m_assembleableID;
  [SerializeField] bool m_isAttach;
  [SerializeField] bool m_grabbedBunble;
  [SerializeField] bool m_withGrabbedSameID;
  [SerializeField] GameObject m_emission;
  [SerializeField] Collider m_trigger;
  public int AssembleableID
  {
    get
    {
      return m_assembleableID;
    }
  }
  public bool IsAttach
  {
    get
    {
      return m_isAttach;
    }
    set
    {
      if (m_isAttach == value)
        return;
      m_isAttach = value;
      UpdateState();
    }
  }
  public bool GrabbedBundle
  {
    get
    {
      return m_grabbedBunble;
    }
    set
    {
      if (m_grabbedBunble == value)
        return;
      m_grabbedBunble = value;
      UpdateState();
    }
  }
  public bool GrabbedSameID
  {
    get
    {
      return m_withGrabbedSameID;
    }
    set
    {
      if (m_withGrabbedSameID == value)
        return;
      m_withGrabbedSameID = value;
      UpdateState();
    }
  }

  private void Start()
  {
    m_emission.SetActive(false);
    m_trigger.enabled = false;
  }

  private void UpdateState()
  {
    if(m_isAttach)
    {
      m_emission.SetActive(false);
    }
    else
    {
      if (m_grabbedBunble)
      {
        if (m_withGrabbedSameID)
        {
          m_emission.SetActive(true);
        }
        else
        {
          m_emission.SetActive(false);
        }
      }
      else
      {
        m_emission.SetActive(false);
      }
    }
  }
}
