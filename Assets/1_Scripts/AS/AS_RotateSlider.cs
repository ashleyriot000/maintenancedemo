﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AS_RotateSlider : MonoBehaviour
{
  public Text text;
  public void RotateBySlide(float point)
  {

    Vector3 e = transform.rotation.eulerAngles;
    e.y = -point * 3.60f + 90f;
    transform.rotation = Quaternion.Euler(e);
    text.text = $"Rotation : {e.y - 90f}";
  }
}
