﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SE_A_ArrowsControl : MonoBehaviour
{
  public bool isOn;
  public Animator[] anims;
  public int WaitTime;
  public int cooltime;

  public void ChangeAnimation()
  {
    isOn = !isOn;
    if (isOn)
    {
      StopAllCoroutines();
      StartCoroutine(Animate());

    }
    else
    {
      StopAllCoroutines();
      StartCoroutine(Deanimate());
    }
  }


  IEnumerator Deanimate()
  {
    for (int i = 0; i < WaitTime; ++i)
    {
      yield return null;
    }
    foreach (var a in anims)
    {
      a.Play("Standby");
      for (int i = 0; i < cooltime; ++i)
      {
        yield return null;

      }
    }
  }

  IEnumerator Animate()
  {
    for (int i = 0; i < WaitTime; ++i)
    {
      yield return null;
    }
    foreach (var a in anims)
    {
      a.SetTrigger("start");
      for (int i = 0; i < cooltime; ++i)
      {
        yield return null;

      }
    }
  }
}
