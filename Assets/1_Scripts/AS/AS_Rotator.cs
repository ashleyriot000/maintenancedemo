﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AS_Rotator : MonoBehaviour
{
  public Text text;
  public bool isOn = false;
  public Transform[] rotates;
  public float[] speeds;
  public float perAdd;
  public float maxSpd;

  public Animator[] anims;
  
  float spdRate;
  bool isActive;
  Renderer[] m_renderers;
  void Update()
  {
    if (isOn)
    {
      if(maxSpd > spdRate)
      {
        spdRate += perAdd;
        if (spdRate >= maxSpd)
        {
          spdRate = maxSpd;
        }
      }
      else
      {
        spdRate -= perAdd;
        if(spdRate <= maxSpd)
        {
          spdRate = maxSpd;
        }
      }
    }
    else
    {
      spdRate -= 0.01f;
      if (spdRate <= 0f)
      {
        spdRate = 0f;
      }
    }

    for (int i = 0; i < rotates.Length; ++i)
    {
      rotates[i].Rotate(Vector3.forward * speeds[i] * spdRate * Time.deltaTime);
    }
  }

  public void ToggleRotation()
  {
    isOn = !isOn;
  }

  public void ChangeSpeed(float spd)
  {
    maxSpd = spd * 0.03f + 1f;
    text.text = $"Speed : {maxSpd}";
    foreach(var a in anims)
    {
      a.SetFloat("Speed", maxSpd);
    }
  }

  public void ToggleActive()
  {
    isActive = !isActive;
    if (isActive) Camera.main.cullingMask |= 1 << 5;
    else Camera.main.cullingMask ^= 1 << 5;
  }
}
