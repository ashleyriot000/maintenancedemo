﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Next_Step : MonoBehaviour
{
    public Animator Anim = null;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Anim.SetTrigger("Next_Step");
        }
    }

}
