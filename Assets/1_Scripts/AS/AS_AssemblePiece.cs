﻿using BNG;
using System.Collections;
using UnityEngine;
[RequireComponent(typeof(Rigidbody), typeof(Grabbable))]
public class AS_AssemblePiece : MonoBehaviour
{
  public enum PieceType
  {
    Assembly,
    Joint,
  }
  public enum FirstState
  {
    Awake,
    Asleep,
  }

  [SerializeField] PieceType m_type;
  [SerializeField] FirstState m_firstState;
  [SerializeField] int m_assembleID;
  
  [SerializeField] bool m_grabbed;
  [SerializeField] bool m_selectedSameID;
  [SerializeField] bool m_relatedGrabbed;
  [SerializeField] bool m_attached;

  [SerializeField] GameObject m_emission;
  [SerializeField] Rigidbody m_rigidbody;
  [SerializeField] Grabbable m_grabbable;
  [Space]
  [SerializeField] FixedJoint m_joint;
  
  //프로퍼티
  public PieceType Type
  {
    get
    {
      return m_type;
    }
  }
  public FirstState First
  {
    get
    {
      return m_firstState;
    }
  }
  public int AssembleID
  {
    get
    {
      return m_assembleID;
    }
  }
  public bool SelectedSameID
  {
    get
    {
      return m_selectedSameID;
    }
    set
    {
      if (m_selectedSameID == value)
        return;
      m_selectedSameID = value;
      UpdateDisplayState();
    }
  }
  public bool Grabbed
  {
    get
    {
      return m_grabbed;
    }
    set
    {
      if (m_grabbed == value)
        return;
      m_grabbed = value;
      UpdateDisplayState();
    }
  }
  public bool RelatedGrabbed
  {
    get
    {
      return m_relatedGrabbed;
    }
    set
    {
      if (m_relatedGrabbed == value)
        return;
      m_relatedGrabbed = value;
      UpdateDisplayState();
    }
  }

  private void Start()
  {
    m_rigidbody = GetComponent<Rigidbody>();
    m_grabbable = GetComponent<Grabbable>();
    if(m_emission == null)
    {
      m_emission = gameObject.transform.Find("Emission").gameObject;
      if (m_emission == null) Debug.LogError("Not Found Emission in Child GameObject");
    }
    m_emission.SetActive(false);

    if (m_firstState != FirstState.Awake)
      Asleep();
  }
  
  private void UpdateDisplayState()
  {
    if (m_grabbed)
    {
      m_emission.SetActive(false);
    }
    else
    {
      if (m_relatedGrabbed & !m_selectedSameID)
      {
        m_emission.SetActive(true);
      }
      else
      {
        m_emission.SetActive(false);
      }
    }
  }
  private void Attach()
  {
    enabled = false;
    
  }

  private void Detach()
  {
    transform.parent = null;
    if (transform.parent != null)
    {
      m_joint = gameObject.GetComponent<FixedJoint>();
      if (m_joint)
      {
        m_joint.connectedBody = null;
        Destroy(m_joint);
      }
    }
  }

  private void SetParent(Transform parent)
  {
    transform.parent = parent;
    Rigidbody parentRB = transform.parent.GetComponent<Rigidbody>();
    if (parentRB)
    {
      FixedJoint joint = gameObject.AddComponent<FixedJoint>();
      joint.autoConfigureConnectedAnchor = true;
      joint.axis = new Vector3(0, 1, 0);
      joint.connectedBody = parentRB;
    }
    StartCoroutine(AlignParent());
  } 

  IEnumerator AlignParent()
  {


    yield return null;
  }

  private void OnTriggerEnter(Collider other)
  {
    var p = other.GetComponent<AS_AssemblePoint>();
    if(p && p.AssembleableID == m_assembleID)
    {
      SetParent(other.transform);
    }
  }
  private void OnTriggerExit(Collider other)
  {
    
  }
  public void Asleep()
  {
    m_grabbable.enabled = false;
  }
  public void Awaken()
  {
    m_grabbable.enabled = true;
  }
  public void OnGrab(bool grabbed)
  {
    Grabbed = grabbed;
    var bundles = FindObjectsOfType<AS_AssembleBundle>();
    foreach(AS_AssembleBundle b in bundles)
    {
      b.RelatedGrabbed = b.RelativeGrab(grabbed, m_assembleID);
    }
    var pieces = FindObjectsOfType<AS_AssemblePiece>();
    foreach(AS_AssemblePiece p in pieces)
    {
      p.SelectedSameID = grabbed;
    }
  }

}
